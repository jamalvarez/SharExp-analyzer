import profanityAnalysis from "profanity-analysis";

export default (text) => profanityAnalysis.analyzeBlob(text);
