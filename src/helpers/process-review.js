import sentimentAnalysis from "../helpers/sentiment-analysis";
import { updateReviewStatusWithSentimentValue } from "../repository/mongo-client";
import { REVIEW_STATUS_APPROVED, REVIEW_STATUS_PENDING } from "../constants";
import profanityAnalysis from "./profanity-analysis";

export default (review) => {
  return new Promise(async (resolve, reject) => {
    try {
      const sentimentResult = sentimentAnalysis(review.reviewText);
      const profanityResult = profanityAnalysis(review.reviewText);
      const reviewStatus = profanityResult.failed
        ? REVIEW_STATUS_PENDING
        : REVIEW_STATUS_APPROVED;
      await updateReviewStatusWithSentimentValue(
        review._id,
        sentimentResult,
        reviewStatus,
        profanityResult.score
      );
      console.log(`Processed review: ${review.entityId}`);
      resolve("OK");
    } catch (error) {
      console.error("Error while processing review: ", review, " error", error);
      reject(error);
    }
  });
};
