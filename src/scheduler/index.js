import { CronJob } from "cron";

let isJobRunning = false;

import { getReviewsByCreatedStatus } from "../repository/mongo-client";
import processReview from "../helpers/process-review";

export const reviewStatusUpdateCronJob = async () => {
  isJobRunning = true;
  while (true) {
    const result = await getReviewsByCreatedStatus();
    console.log("totalCount::", result.totalCount);
    if (!result.totalCount) {
      console.log("No reviews with Created status");
      isJobRunning = false;
      break;
    }
    const processArray = result.reviews.map((review) => processReview(review));
    await createBatchProcess(processArray);
  }
};

const createBatchProcess = (processArray) => {
  return new Promise((resolve) => {
    Promise.all(processArray).then(() => {
      resolve("Ok");
    });
  });
};

export const job = new CronJob(
  process.env.CRON_JOB_PATTERN,
  async () => {
    console.log("Running cron job at:", new Date());
    if (!isJobRunning) {
      await reviewStatusUpdateCronJob();
    }
  },
  null,
  true,
  "America/Santiago"
);
