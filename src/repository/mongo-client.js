import { MongoClient } from "mongodb";
import {
  RATINGS_COLUMN_MAPPING,
  REVIEW_STATUS_CREATED,
  SORT_OPTIONS
} from "../constants";

require("dotenv").config();

let dbConnection, mongoClient;

MongoClient.connect(process.env.MONGO_DB_URL, function (err, client) {
  if (err) {
    console.error("error while connecting to mongodb::: ", err);
  } else {
    mongoClient = client;
    dbConnection = client.db(process.env.MONGO_DATABASE);
    console.log("Connected to mongodb server");
  }
});

const getReviewsByCreatedStatus = async () => {
  const listCursor = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .find({
      reviewStatus: REVIEW_STATUS_CREATED
    })
    .limit(parseInt(process.env.CRON_JOB_REVIEWS_PROCESS_LIMIT || 10, 10));
  const totalCount = await listCursor.count();
  const reviews = await listCursor.toArray();
  return { totalCount, reviews };
};

const updateReviewStatusWithSentimentValue = async (
  id,
  sentimentAnalysisScore,
  reviewStatus,
  profanityAnalysisScore
) =>
  new Promise((resolve, reject) => {
    dbConnection.collection(`${process.env.ENTITY}_reviews`).findOneAndUpdate(
      {
        _id: id
      },
      {
        $set: { reviewStatus, sentimentAnalysisScore, profanityAnalysisScore }
      },
      {},
      function (err, doc) {
        if (err) {
          reject(err);
        } else {
          resolve("Updated");
        }
      }
    );
  });

export {
  mongoClient,
  dbConnection,
  getReviewsByCreatedStatus,
  updateReviewStatusWithSentimentValue
};
