# E-commerce - Reviews & Ratings

![Generic badge](https://img.shields.io/badge/USES-JS-yellow.svg?style=for-the-badge)

> Batch Jobs for Falabella Reviews & Ratings
> Background batch process that takes the newly created reviews, runs them through sentiment and profanity analysis and updates the table with corresponding values.


## Prerequisites

- NodeJS v10.16.0 and above
- Mongodb v3.6.2 and above

## Installation

To get started with the project, first **clone** the project from gitlab

```
git@git.fala.cl:hpagadimarri/hackathon2020-batchjob.git
```

after cloning

```
$ cd hackathon2020-batchjob
``` 

Package installation

```
npm install
``` 

After the above command runs successfully, the **dependencies** get installed


To run dev

```
npm run dev
``` 

To run build

```
npm run build
``` 

To run start

```
npm run start
``` 

``` 
## Swagger Doc
https://app.swaggerhub.com/apis/hpagadimarri/ratings-and_reviews/2.0
